<?php

namespace Drupal\quicktwig\Form;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class QuickTwigForm.
 */
class QuickTwigForm extends FormBase {
  /**
   * File system interface needed to save Twig template.
   *
   * @var Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Guzzle client interface needed to fetch HTML source code.
   *
   * @var GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Select options for New Template Filename field.
   *
   * @var array
   */
  protected $selectOptions;

  /**
   * Select options for Theme Hook field.
   *
   * @var array
   */
  protected $selectedThemeHook;

  /**
   * Select options for Source Template field.
   *
   * @var array
   */
  protected $selectedSourceTwig;

  /**
   * Select options for New Template Filename field.
   *
   * @var array
   */
  protected $selectedDestinationTwig;

  /**
   * Class constructor.
   */
  public function __construct(ClientInterface $http_client, FileSystemInterface $fileSystem) {
    $this->httpClient = $http_client;
    $this->selectOptions = $this->getAllSelectOptions();
    $this->selectedThemeHook = key($this->getThemeHookOptions());
    $this->selectedSourceTwig = key($this->getSourceTwigOptions($this->selectedThemeHook));
    $this->selectedDestinationTwig = key($this->getDestinationTwigOptions($this->selectedSourceTwig));
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'quicktwig_form';
  }

  /**
   * Fetch html source code of current page, parse twig filenames and compile into an array.
   *
   * @return array
   *   Return array of select options for form fields.
   */
  public function getAllSelectOptions() {
    global $base_url;
    $path = $this->getRedirectDestination()->get();
    $selectOptions = [];

    try {
      // Fetch html source code of current page.
      $response = $this->httpClient->get($base_url . $path);
      $html = (string) $response->getBody();

      // Find twig debug code.
      if (preg_match_all("/<!-- THEME HOOK: '(.*?)' -->(?:\n?<!-- FILE NAME SUGGESTIONS:\n?(.*?)\n?-->)?\n?<!-- BEGIN OUTPUT from '(.*?)' -->/s", $html, $matches, PREG_SET_ORDER)) {

        foreach ($matches as $match) {
          // Parse file name suggestions from string to array.
          if (!empty($match[2])) {
            // Convert file name suggestions into array, and remove extra
            // characters and spaces.
            $destination_twig = array_map(function ($i) {
              return preg_replace(['/\*\s/', '/x\s/', '/\s/'], '', $i);
            }, preg_split("/\r?\n/", $match[2]));
          }
          else {
            $destination_twig = [];
          }

          // Build select options array for all form fields.
          $key = array_search($match[3], array_column($selectOptions, 'source_twig'));
          if ($key === FALSE) {
            $selectOptions[] = [
              'theme_hook' => $match[1],
              'source_twig' => $match[3],
              'destination_twig' => $destination_twig,
            ];
          }
          else {
            $selectOptions[$key]['destination_twig'] = array_unique(array_merge($selectOptions[$key]['destination_twig'], $destination_twig));
          }
        }
      }
    }
    catch (RequestException $e) {
      watchdog_exception('quicktwig', $e->getMessage());
    }

    return $selectOptions;
  }

  /**
   * Return select options for Theme Hook field.
   *
   * @return array
   *   Return array of select options.
   */
  public function getThemeHookOptions() {
    $theme_hook_array = array_unique(array_column($this->selectOptions, 'theme_hook'));
    return array_combine($theme_hook_array, $theme_hook_array);
  }

  /**
   * Return select options for Source Template field.
   *
   * @return array
   *   Return array of select options.
   */
  public function getSourceTwigOptions($theme_hook) {
    $keys = array_keys(array_column($this->selectOptions, 'theme_hook'), $theme_hook);
    $source_twig_array = [];

    if (!empty($keys)) {
      foreach ($keys as $key) {
        $source_twig_array[] = $this->selectOptions[$key]['source_twig'];
      }

      return array_combine($source_twig_array, $source_twig_array);
    }
    else {
      return [];
    }
  }

  /**
   * Return select options for New Template Filename field.
   *
   * @return array
   *   Return array of select options.
   */
  public function getDestinationTwigOptions($source_twig) {
    $key = array_search($source_twig, array_column($this->selectOptions, 'source_twig'));

    if ($key !== FALSE) {
      $destination_twig_array = $this->selectOptions[$key]['destination_twig'];
      return array_combine($destination_twig_array, $destination_twig_array);
    }
    else {
      return [];
    }
  }

  /**
   * Build QuickTwig form.
   *
   * @return array
   *   Return form array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;

    $form['quicktwig_fieldset'] = [
      '#type' => 'fieldset',
      '#prefix' => '<div id="quicktwig-fieldset">',
      '#suffix' => '</div>',
    ];

    // Set default Theme Hook value to first entry, and set other field default
    // values to NULL if Theme Hook is changed.
    if ($this->selectedThemeHook === NULL) {
      $this->selectedThemeHook = key($this->getThemeHookOptions());
    }
    elseif ($this->selectedThemeHook !== $form_state->getValue(['quicktwig_fieldset', 'theme_hook']) && $form_state->hasValue(['quicktwig_fieldset', 'theme_hook'])) {
      $this->selectedThemeHook = $form_state->getValue(['quicktwig_fieldset', 'theme_hook']);
      $this->selectedSourceTwig = NULL;
      $this->selectedDestinationTwig = NULL;
    }

    $form['quicktwig_fieldset']['theme_hook'] = [
      '#type' => 'select',
      '#title' => $this->t('Theme Hook'),
      '#options' => $this->getThemeHookOptions(),
      '#default_value' => $this->selectedThemeHook,
      '#ajax' => [
        'callback' => '::ajaxCallback',
        'event' => 'change',
        'wrapper' => 'quicktwig-fieldset',
      ],
    ];

    // Set default Source Template value to first entry, and set New Template
    // Filename to NULL if Source Template is changed.
    if ($this->selectedSourceTwig === NULL) {
      $this->selectedSourceTwig = key($this->getSourceTwigOptions($this->selectedThemeHook));
    }
    elseif ($this->selectedSourceTwig !== $form_state->getValue(['quicktwig_fieldset', 'source_twig']) && $form_state->hasValue(['quicktwig_fieldset', 'source_twig'])) {
      $this->selectedSourceTwig = $form_state->getValue(['quicktwig_fieldset', 'source_twig']);
      $this->selectedDestinationTwig = NULL;
    }

    $form['quicktwig_fieldset']['source_twig'] = [
      '#type' => 'select',
      '#title' => $this->t('Source Template'),
      '#options' => $this->getSourceTwigOptions($this->selectedThemeHook),
      '#default_value' => $this->selectedSourceTwig,
      '#ajax' => [
        'callback' => '::ajaxCallback',
        'event' => 'change',
        'wrapper' => 'quicktwig-fieldset',
      ],
    ];

    // Set default New Template Filename value to first entry.
    if ($this->selectedDestinationTwig === NULL) {
      $this->selectedDestinationTwig = key($this->getDestinationTwigOptions($this->selectedSourceTwig));
    }
    elseif ($this->selectedDestinationTwig !== $form_state->getValue(['quicktwig_fieldset', 'destination_twig']) && $form_state->hasValue(['quicktwig_fieldset', 'destination_twig'])) {
      $this->selectedDestinationTwig = $form_state->getValue(['quicktwig_fieldset', 'destination_twig']);
    }

    $theme = $this->config('system.theme')->get('default');
    $templates_path = DRUPAL_ROOT . '/' . drupal_get_path('theme', $theme) . '/templates/';

    $form['quicktwig_fieldset']['destination_twig'] = [
      '#type' => 'select',
      '#title' => $this->t('New Template Filename'),
      '#description' => $this->t('All Twig files will be copied to %templates_path', ['%templates_path' => $templates_path]),
      '#options' => $this->getDestinationTwigOptions($this->selectedSourceTwig),
      '#default_value' => $this->selectedDestinationTwig,
    ];

    $form['quicktwig_fieldset']['actions'] = [
      '#type' => 'actions',
    ];

    $form['quicktwig_fieldset']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => ['button--primary'],
      ],
    ];

    return $form;
  }

  /**
   * Ajax callback.
   */
  public function ajaxCallback(array &$form, FormStateInterface $form_state) {
    return $form['quicktwig_fieldset'];
  }

  /**
   * Process form submission values.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $source_twig = $form_state->getValue(['quicktwig_fieldset', 'source_twig']);
    $destination_twig = $form_state->getValue(['quicktwig_fieldset', 'destination_twig']);
    $theme = $this->config('system.theme')->get('default');

    // Determine path of source twig file to override and destination of current
    // theme's templates folder.
    $source_path = DRUPAL_ROOT . '/' . $source_twig;
    $destination_path = DRUPAL_ROOT . '/' . drupal_get_path('theme', $theme) . '/templates/' . $destination_twig;

    if ($this->fileSystem->copy($source_path, $destination_path) !== FALSE) {
      $this->messenger()->addStatus($this->t('Twig template was successfully copied from %source to %destination.', [
        '%source' => $source_path,
        '%destination' => $destination_path,
      ]));
    }
    else {
      $this->messenger()->addError($this->t('Twig template could not be copied from %source to %destination.', [
        '%source' => $source_path,
        '%destination' => $destination_path,
      ]));
    }
  }

}
