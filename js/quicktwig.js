(function($, Drupal) {
    Drupal.behaviors.quicktwig = {
      attach: function (context, settings) {
        $(".form-item-quicktwig-fieldset-source-twig, .form-item-quicktwig-fieldset-destination-twig").hide();
        $("#quicktwig-fieldset .form-submit").attr('disabled', 'disabled');

        $.get(window.location.href, function(html) {
          const regex = new RegExp(/<!-- THEME HOOK: '(.*?)' -->(?:\n?<!-- FILE NAME SUGGESTIONS:\n?(.*?)\n?-->)?\n?<!-- BEGIN OUTPUT from '(.*?)' -->/gs);
          const options = [];
          const theme_hook_map = new Map();
          const source_twig_map = new Map();
          let destination, twig;

          while((match = regex.exec(html)) != null) {
            if (match[2]) {
              destination = match[2].split(/\r?\n/).map(function(i) {
                return i.replace(/\*\s/g, '').replace(/x\s/g, '').replace(/\s/g, '');
              });
            } else {
              destination = [];
            }

            if (!theme_hook_map.has(match[1])) {
              theme_hook_map.set(match[1], true);

              $("select[name='quicktwig_fieldset[theme_hook]']", context).append('<option value="' + match[1] + '">' + match[1] + '</option>');
            }

            if (!source_twig_map.has(match[3])) {
              source_twig_map.set(match[3], true);

              options.push({
                theme_hook: match[1],
                source_twig: match[3],
                destination_twig: destination
              });
            } else {
              twig = options.find(i => i.source_twig == match[3]);

              if (twig.destination_twig.length > 0) {
                for (i=0; i<destination.length; i++) {
                  if (twig.destination_twig.indexOf(destination[i]) === -1) {
                    twig.destination_twig.push(destination[i]);
                  }
                }
              } else if (destination.length > 0) {
                twig.destination_twig.push(destination);
              }
            }
          }

          $("select[name='quicktwig_fieldset[theme_hook]']", context).change(function() {
            $("select[name='quicktwig_fieldset[source_twig]']", context).empty();

            for (i=0; i<options.length; i++) {
              if ($("select[name='quicktwig_fieldset[theme_hook]'] option:selected", context).val() == options[i].theme_hook) {
                $("select[name='quicktwig_fieldset[source_twig]']", context).append('<option value="' + options[i].source_twig + '">' + options[i].source_twig + '</option>');
              }
            }

            if ($("select[name='quicktwig_fieldset[source_twig]'] option", context).length > 0) {
              $(".form-item-quicktwig-fieldset-source-twig, .form-item-quicktwig-fieldset-destination-twig").show();
              $("select[name='quicktwig_fieldset[source_twig]']", context).trigger('change');
            } else {
              $(".form-item-quicktwig-fieldset-source-twig, .form-item-quicktwig-fieldset-destination-twig").hide();
              $("#quicktwig-fieldset .form-submit").attr('disabled', 'disabled');
            }
          });

          $("select[name='quicktwig_fieldset[source_twig]']", context).change(function() {
            $("select[name='quicktwig_fieldset[destination_twig]']", context).empty();

            let twig = options.find(i => i.source_twig == $("select[name='quicktwig_fieldset[source_twig]'] option:selected", context).val());

            if (twig.destination_twig.length > 0) {
              for (i=0; i<twig.destination_twig.length; i++) {
                $("select[name='quicktwig_fieldset[destination_twig]']", context).append('<option value="' + twig.destination_twig[i] + '">' + twig.destination_twig[i] + '</option>');
              }

              $(".form-item-quicktwig-fieldset-destination-twig").show();
              $("#quicktwig-fieldset .form-submit").removeAttr('disabled');
            } else {
              $(".form-item-quicktwig-fieldset-destination-twig").hide();
              $("#quicktwig-fieldset .form-submit").attr('disabled', 'disabled');
            }
          });

          $("select[name='quicktwig_fieldset[theme_hook]']", context).trigger('change');
        });
      }
    };

})(jQuery, Drupal);
