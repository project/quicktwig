# QUICKTWIG

## INTRODUCTION

**QuickTwig** is a tool for theme developers to quickly override Twig template
files.  Instead of searching through Twig debug markup and manually copying
and renaming files to your theme folder, you can easily access QuickTwig on
the toolbar, which will open an off-canvas dialog on the page.  Select a theme
hook, source template and new template filename (based on the file name
suggestions listed in the HTML comments), and QuickTwig will copy the Twig
template to your current theme.

For a full description of the module, visit the project page:
[https://www.drupal.org/project/quicktwig](https://www.drupal.org/project/quicktwig)

To submit bug reports and feature suggestions, or track changes:
[https://www.drupal.org/project/issues/quicktwig](https://www.drupal.org/project/issues/quicktwig)

## REQUIREMENTS

Twig debugging must be enabled in *sites/default/services.yml* to use QuickTwig.

## INSTALLATION

* Install as you would normally install a contributed Drupal module. Visit
[https://www.drupal.org/docs/user_guide/en/extend-module-install.html](https://www.drupal.org/docs/user_guide/en/extend-module-install.html)
for further information.

## CONFIGURATION

No configuration needed.

## LIMITATIONS AND TODO

* Currently doesn't work for any internal admin pages and any authenticated
content
* Ability to enter custom template filename
* Ability to select destination templates directory
* Add Tests
* Other ideas?

## MAINTAINERS

* Ray Yick ([dystopianblue](https://www.drupal.org/u/dystopianblue))
